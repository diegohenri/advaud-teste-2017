# Teste de tratamento de texto#

Bem-vindo ao teste prático de programação de nível basico.

### Sobre o teste ###

Consiste em formatar um texto com marcações e transforma-lo em blocos individuais para facilitar a leitura do usuário.

### Instruções ###

* Faça um fork desse projeto no BitBucket;
* Expanda um commit a cada alteração significativa, para pontuar as alterações;
* Após a conclusão do teste, faça um *Pull Request* para o projeto principal;
* O arquivo de texto a ser formatado, se encontra em `storage/summary.txt`; 
* O texto contém marcações como `==STARTROW==` e `==ENDROW==`. *Essas marcações identificam o início e o final da linha respectivamente.*
* Será necessário separar os atributos de cada linha como **Nome**, **RF** e **Cargo/Função**;
* Concatenar o cabeçalho - o texto que antecede as linhas;

Segue o exemplo de como ficará o texto após a correta formatação:

	1.
	HORAS SUPLEMENTARES DE TRABALHO
	Face o disposto no art. 138, da Lei 11.511, de 19.04.94, 
	regulamentada pelo Decreto 34.781/94 de 22.12.94, bem como 
	na Portaria nº 008/SMA-G/95, e o disposto da Lei 13.399 de 
	01.08.02 e da Portaria 046/SMSP/GAB/2016, e a autorização do 
	Prefeito Regional, segue abaixo relacionado os servidores con-
	vocados para prestação de Horas Suplementares de Trabalho.
	PARA O PERÍODO DE 01/08/2017 A 31/08/2017

	NOME: ADALBERTO RODOLFO AGUIAR E SILVA
	RF: 581.446.4
	CARGO/FUNÇÃO: AG. APOIO
	
	2.
	HORAS SUPLEMENTARES DE TRABALHO
	Face o disposto no art. 138, da Lei 11.511, de 19.04.94, 
	regulamentada pelo Decreto 34.781/94 de 22.12.94, bem como 
	na Portaria nº 008/SMA-G/95, e o disposto da Lei 13.399 de 
	01.08.02 e da Portaria 046/SMSP/GAB/2016, e a autorização do 
	Prefeito Regional, segue abaixo relacionado os servidores con-
	vocados para prestação de Horas Suplementares de Trabalho.
	PARA O PERÍODO DE 01/08/2017 A 31/08/2017
	
	NOME: ALESSANDRA AGUIAR BORGES LAZO
	RF: 711.043.0
	CARGO/FUNÇÃO: AG. APOIO

	3.
	...

* Você não precisa fazer nenhuma interface para apresentação, apenas deixe claro como executar o script corretamente.

### Métodologia e estrutura do projeto ###

* Tente utilizar algum *Design Pattern* para o desenvolvimento desse projeto;
* Fique a vontade para utilizar algum framework de sua preferência. *Não será considerado um diferencial;*
####Diferenciais:
* Programação orientada a objetos;
* Utilização do composer para namespaces;
* Testes unitários com o PHPUnit;
* Comentários nos métodos e classes para manutenção de código, utilizando os padrões.
* Estrutura bem definida de diretórios e classes;

##Observações##
Caso tenha alguma dúvida, abra uma nova issue ou mande um e-mail para: diego.henrique@advaud.com.br

Bom teste.

